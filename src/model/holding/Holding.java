package model.holding;

import model.utilities.DateTime;

public abstract class Holding 
{
	/*
	 * seven-digit alpha-numeric code prefixed with �b� for a book and �v� for
	 * a video (e.g. b000010 or v000010);
	 */
	private String holdingId;
	
	// Title must be at least one character in length;
	private String title;
	
	// predefined loan fee ($ value);
	protected double loanFee;
	
	// maximum loan period (defined in terms of the number of days);
	protected int maxLoanPeriod;
	
	// late penalty fee ($ value) calculated on a per late day basis.
	protected double lateFee;
	
	// an attribute which indicates if the holding has been temporarily removed from the system.
	protected boolean removed;
	
	/*
	 * A holding may be made �inactive� essentially preventing the item from
	 * being borrowed. The conditions that make a holding inactive are:
	 * # If a holding is in an invalid state such as having an invalid ID
	 * 		or title, it should be marked as �inactive�.
	 * # If a holding is damaged, then the administrator of the system can
	 * 		mark the item as �inactive�.
	 */
	protected boolean inactive;
	
	// Date holding was borrowed
	private DateTime borrowDate;
	
// ------ CLASS FUNCTIONS ---------------------------------------------------------
	
	public Holding(String holdingId, String title) 
	{
		this.holdingId = holdingId;
		this.title = title;
	}
	
	public String getID()
	{
		return holdingId;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	// Should return if the holding is active or inactive
	public boolean getStatus()
	{
		return inactive;
	}

	public boolean isOnLoan()
	{
		if ( borrowDate != null )
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public DateTime getBorrowDate()
	{
		return borrowDate;
	}
	
	public int getDefaultLoanFee()
	{
		//TODO
		return -1;
	}
	
	public int getMaxLoanPeriod()
	{
		return maxLoanPeriod;
	}
	
	public int calculateLateFee(DateTime dateReturned)
	{
		//TODO
		return -1;
	}
	
	/*
	 * A holding can only be borrowed if:
	 * # it is currently active in the system
	 * # it is not already on loan
	 * If an item is borrowed, then it must have it�s borrowDate set to the current date
	 */
	public boolean borrowHolding()
	{
		//TODO
		return false;
	}
	
	/*
	 * A holding can only be returned if:
	 * # it is currently active in the system
	 * # it is already on loan
	 * # the return date is on or after the date on which the item was borrowed
	 * If an item is returned, then it must have it�s borrowDate set to null
	 */
	public boolean returnHolding(DateTime dateReturned)
	{
		//TODO
		return false;
	}
	
	/*
	 * The print method should display the current state of the object in a user friendly format designed for display on the console.
	For example:
	ID: b000001
	Title: Introduction to Java Programming
	Loan Fee: 10
	Max Loan Period: 28
	*/
	public void print()
	{
		//TODO
	}
	
	/*
	 * The Holding classes should override the toString() method to provide a 
	 * pre-determined string representation of the holding. The format for the 
	 * holding representation separates each attribute via the use of a colon �:�
	 * holding_code:holding_title:standard_loan_fee:max_loan_period
	 * e.g. b000001:Intro to Java:10:28 
	 * 		v000001:Intro to Java 1:4:7
	 */
	public String toString()
	{
		//TODO
		return " ";
	}

	public boolean activate()
	{
		//TODO;
		return false;
	}
	
	public boolean deactivate()
	{
		//TODO;
		return false;
	}
}
