package model.holding;

public class Video extends Holding 
{
	/*
	 * late fee = number of late days x 50% of the standard loan fee.
	 * e.g. if a given video with a standard loan fee of $6 was returned
	 * 		3 days late, the late fee will be $9 (3 days x 50% of $6).
	 */

	public Video(String holdingId, String title, int loanFee) 
	{
		super(holdingId, title);

		// Videos have a variable loan fee of either $4 or $6   TODO WTF!!!!!
		super.loanFee = loanFee;  
		
		// Videos have a fixed maximum loan period of 7 days
		super.maxLoanPeriod = 7;
	}
}
