package model.holding;

public class Book extends Holding 
{
	/*
	 * late fee = number of late days x fixed daily rate of $2. 
	 * e.g. if a given book was returned 3 days late, the late fee
	 * 		will be $6 (3 days x $2). 
	 */
		
	public Book(String holdingId, String title)
	{
		super(holdingId, title);
		
		// Books have a fixed standard loan fee of $10
		super.loanFee = 10;
		
		// Books have a fixed maximum loan period of 28 days
		super.maxLoanPeriod = 28;
	}
}
