package model.member;

import model.holding.Holding;

public abstract class Member 
{
	/*
	 * seven-digit alpha-numeric code prefixed with �s� for a standard member
	 *  and �p� for a premium member (e.g. s000010 or p000010);
	 */
	private String memberId;
	
	// full name (e.g. �John Smith�) which must be at least one character in length
	private String fullName;
	
	// maximum borrowing credit ($ value) which is predefined according to the specific membership type
	// Each member should have their credit set to the maximum available credit limit for that type of member when the member is created.
	/*
	 * 		Standard members have a fixed maximum borrowing credit of $30
			Premium members have a fixed maximum borrowing credit of $45.
	 */
	private int maxBorrowCredit;  //TODO

	
	// collection of currently borrowed holdings
	private Holding[] borrowedItems;
	
	
	
/*
	# The holding�s standard loan fee should be deducted from member�s borrowing credit at the time he/she borrows a holding.
	# The member must have sufficient credit available to borrow a given holding.
	# The holding return procedure differs based on a specific member type:
		## Standard members have a fixed maximum borrowing credit of $30
		## Premium members have a fixed maximum borrowing credit of $45.
			
			*/
	
	public Member(String memberID, String fullName, int credit)
	{
		this.memberId = memberID;
		this.fullName = fullName;
		this.maxBorrowCredit = credit;
	}

}
