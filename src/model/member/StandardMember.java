package model.member;

public class StandardMember extends Member 
{
	// Standard members have a fixed maximum borrowing credit of $30
	final static int MAXBORROWCREDIT = 30;
	
	public StandardMember(String standardMemberId, String standardMemberName)
	{
		super(standardMemberId, standardMemberName, MAXBORROWCREDIT);
	}
}
