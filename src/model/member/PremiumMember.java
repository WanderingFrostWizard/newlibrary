package model.member;

public class PremiumMember extends Member
{
	public PremiumMember(String premimumMemberId, String premiumMemberName) 
	{ 
		// Premium members have a fixed maximum borrowing credit of $45
		super(premimumMemberId, premiumMemberName, 45);
	}
}
