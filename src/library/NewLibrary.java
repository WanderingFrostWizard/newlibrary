package library;

/**
 * Attempt to re-create an assignment from the beginning of my programming degree
 * Semester 2 2016 - Programming Techniques Assignment 2
 * 
 * Program is designed to be a library database, that stores information about 
 * 1. The library and its collection
 * 2. The holdings contained in the library collection
 * 3. The members who can borrow/return available holdings
 * 
 * @author Frosty
 *
 */
public class NewLibrary 
{

	public static void main(String[] args) 
	{

	}

}
